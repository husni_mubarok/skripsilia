	<?php
	include_once 'Database.php';
	class rptpemakaianbenang {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					DATE_FORMAT(
						`order`.tgltrans,
						'%d/%m/%Y'
					) AS tgltrans,
					CONCAT(
						barang.kodebarang, ' ',
						barang.namabarang, ' ',
						warnabarang.namawarnabarang
					) AS jenisbenang,
					ukuranbarang.namaukuranbarang,
					tipebarang.namatipebarang,
					SUM(orderdetail.jumlah) AS jumlah
				FROM
					`order`
				INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
				LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
				LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
				LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
				LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang 
				WHERE `order`.tgltrans BETWEEN :rptdatefrom AND :rptdateto 
				GROUP BY
					`order`.tgltrans,
					barang.kodebarang,
					barang.namabarang,
					tipebarang.namatipebarang,
					warnabarang.namawarnabarang,
					ukuranbarang.namaukuranbarang";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
					SUM(orderdetail.jumlah) AS tjumlah
				FROM
					`order`
				INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
				LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
				LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
				LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
				LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
				WHERE `order`.tgltrans BETWEEN :rptdatefrom AND :rptdateto";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }
  
}
?>

