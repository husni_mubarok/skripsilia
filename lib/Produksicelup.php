<?php
include_once 'Database.php';
class produksicelup {
    private $db = '';
    private $data;
	private $table = 'purchaseorder';
    public function __construct() {
        $this->db = new Database();
    }

	function showOpenDetail($key) {
        $sql = "SELECT
					`order`.idtrans,
					`order`.kodetrans,
					`order`.notrans,
					DATE_FORMAT(`order`.tgltrans, '%d/%m/%Y')AS tgltrans,
					`order`.keterangan AS keteranganh,
					`order`.pelangganid,
					pelanggan.namapelanggan,
					orderdetail.idtransdet,
					orderdetail.transid,
					orderdetail.barangid,
					orderdetail.satuan,
					orderdetail.jumlah,
					barang.kodebarang,
					barang.namabarang,
					barang.tipebarangid,
					tipebarang.namatipebarang,
					barang.warnabarangid,
					warnabarang.namawarnabarang,
					ukuranbarang.namaukuranbarang,
					barang.ukuranbarangid,
					DATE_FORMAT(
						`orderdetail`.jadwalcelup,
						'%m/%d/%Y'
					)AS jadwalcelup,
					IFNULL(orderdetail.jumlah, 0)- IFNULL(produksicelupdetail.jumlah, 0) AS jumlahsisa
				FROM
					`order`
				INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
				INNER JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
				LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
				LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
				LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
				LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
				LEFT JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
				WHERE
					IFNULL(orderdetail.jumlah, 0)- IFNULL(produksicelupdetail.jumlah, 0)> 0";
        $arrData = array(':key' => $key);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showListHeader($key) {
        $sql = "SELECT
				produksicelup.idtrans,
				produksicelup.kodetrans,
				produksicelup.notrans,
				DATE_FORMAT(produksicelup.tgltrans,'%d/%m/%Y') AS tgltrans,
				produksicelup.keterangan AS keteranganh 
				FROM
				produksicelup
				WHERE  produksicelup.idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->getById($sql, $arrData);
        return $this->data;
    }

    function pickheader($kodetrans, $notrans, $tgltrans) {
        $sql = "INSERT INTO produksicelup (kodetrans, notrans, tgltrans)
				SELECT :kodetrans,
				IFNULL(CONCAT(:kodetrans,'-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(produksicelup.notrans),3))+1001,3)), CONCAT(:kodetrans,'-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())
				FROM
				produksicelup
				WHERE kodetrans=:kodetrans";
        $arrData = array(':kodetrans' => $kodetrans, ':notrans' => $notrans, ':tgltrans' => $tgltrans, ':supplierid' => $supplierid);
        $this->data = $this->db->insertData($sql, $arrData);
		if($this->data == true){
			$key = $this->db->lastID();
            header("location:index.php?mod=produksicelup_ldetail&key=$key");
        }		
    }

	function pickdetail($idtransdet, $totalpick) {
		$key = $this->db->lastID();		
		$count = count($_POST['checkedid']);
		
		for($i=0;$i<=$count;$i++) {
		$idtransdet= $_POST['checkedid'][$i];
		$totalpick= $_POST['totalpick'][$i];
        $sql = "INSERT INTO produksicelupdetail (orderdetailid, transid, barangid, satuan, jumlah)
				SELECT
				:idtransdet,
				'".$key."',
				orderdetail.barangid,
				orderdetail.satuan,
				:totalpick 
				FROM
				orderdetail WHERE orderdetail.idtransdet = '".$idtransdet."'";
        $arrData = array(':idtransdet' => $idtransdet, ':totalpick' => $totalpick);
        $this->data = $this->db->insertData($sql, $arrData);
		} 
		if ($this->data == true) {
            
        }
		return $this->data;
    } 
    function saveclose($key, $tgltrans, $keterangan) {
		$vardatetime = $_POST['tgltrans']; 
		$date = str_replace('/', '-', $vardatetime);
		$tgltrans = date('Y-m-d', strtotime($date));
				
        $sql = "UPDATE produksicelup SET tgltrans=:tgltrans, keterangan=:keterangan WHERE idtrans=:key";
        $arrData = array(':tgltrans' => $tgltrans, ':keterangan' => $keterangan, ':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=produksicelup");
        }
        return $this->data;
    }

    function savefail($key, $tgltrans, $keterangan) {
		$vardatetime = $_POST['tgltrans']; 
		$date = str_replace('/', '-', $vardatetime);
		$tgltrans = date('Y-m-d', strtotime($date));
				
        $sql = "UPDATE produksicelup SET tgltrans=:tgltrans, keterangan=:keterangan, status=9 WHERE idtrans=:key";
        $arrData = array(':tgltrans' => $tgltrans, ':keterangan' => $keterangan, ':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=produksicelup");
        }
        return $this->data;
    }

}
?>
