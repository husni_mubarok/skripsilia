<?php
include_once 'Database.php';
date_default_timezone_set("Asia/Bangkok");
class order {
    private $db = '';
    private $data;
    public function __construct() {
        $this->db = new Database();
    }
    function insert($kodetrans) {
        $sql = "INSERT INTO `order` (kodetrans, notrans, tgltrans)
				SELECT :kodetrans,
				IFNULL(CONCAT(:kodetrans,'-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(`order`.notrans,3)),3))+1001,3)), CONCAT(:kodetrans,'-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
				FROM
				`order`
				WHERE kodetrans=:kodetrans";
        $arrData = array(':kodetrans' => $kodetrans);
        $this->data = $this->db->insertData($sql, $arrData);
		if($this->data == true){
			$key = $this->db->lastID();
            header("location:index.php?mod=order_detail&key=$key");
        }		
    }
	
    function saveclose($key, $tgltrans, $pelangganid, $keterangan) {
		$vardatetime = $_POST['tgltrans']; 
		$date = str_replace('/', '-', $vardatetime);
		$tgltrans = date('Y-m-d', strtotime($date));
        $sql = "UPDATE `order` SET tgltrans=:tgltrans, pelangganid=:pelangganid, keterangan=:keterangan WHERE idtrans=:key";
        $arrData = array(':tgltrans' => $tgltrans, ':pelangganid' => $pelangganid, ':keterangan' => $keterangan, ':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=order");
        }
        return $this->data;
    }

    function cancel($key) {
        $sql = "UPDATE `order` SET status=9 WHERE idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=order");
        }
        return $this->data;
    }

    function showDetail($key) {
        $sql = "SELECT
				`order`.idtrans,
				`order`.kodetrans,
				`order`.notrans,
				DATE_FORMAT(`order`.tgltrans,'%d/%m/%Y') AS tgltrans,
				`order`.keterangan AS keteranganh,
				`order`.pelangganid,  
				pelanggan.namapelanggan
				FROM
				`order`
				LEFT JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
				WHERE `order`.idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->getById($sql, $arrData);
        return $this->data;
    }

	function delete($key) {
        $sql = "DELETE FROM `order` WHERE idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->deleteData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=order");
        }
    }

}
?>
