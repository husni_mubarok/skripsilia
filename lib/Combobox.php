<?php
include_once 'Database.php';
class combobox {
    private $db = '';
    private $data;
    public function __construct() {
        $this->db = new Database();
    }
    function showPeriod() {
        $sql = "SELECT * FROM period WHERE status=0";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showUsers() {
        $sql = "SELECT * FROM users";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
	function showTicketStatus() {
        $sql = "SELECT * FROM ticketstatus WHERE status=0";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
	function showAttribute() {
        $sql = "SELECT * FROM attribute WHERE status=0";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
	function showPelanggan() {
        $sql = "SELECT * FROM pelanggan";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showTipepelanggan() {
        $sql = "SELECT * FROM tipepelanggan";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showKota() {
        $sql = "SELECT * FROM kota";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showTipebarang() {
        $sql = "SELECT * FROM tipebarang";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showUkuranbarang() {
        $sql = "SELECT * FROM ukuranbarang";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showWarnabarang() {
        $sql = "SELECT * FROM warnabarang";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
}
?>
