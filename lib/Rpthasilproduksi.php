	<?php
	include_once 'Database.php';
	class rpthasilproduksi {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					`order`.idtrans,
					`order`.kodetrans,
					`order`.notrans,
					DATE_FORMAT(`order`.tgltrans, '%d/%m/%Y')AS tgltrans,
					`order`.keterangan AS keteranganh,
					`order`.pelangganid,
					pelanggan.namapelanggan,
					orderdetail.idtransdet,
					orderdetail.transid,
					orderdetail.barangid,
					orderdetail.satuan,
					orderdetail.jumlah,
					barang.kodebarang,
					barang.namabarang,
					barang.tipebarangid,
					tipebarang.namatipebarang,
					barang.warnabarangid,
					warnabarang.namawarnabarang,
					ukuranbarang.namaukuranbarang,
					barang.ukuranbarangid,
					DATE_FORMAT(
						`orderdetail`.jadwalcelup,
						'%m/%d/%Y'
					)AS jadwalcelup,
					IFNULL(orderdetail.jumlah, 0)- IFNULL(produksicelupdetail.jumlah, 0) AS jumlahsisa,
					produksicelup.notrans AS noproduksi,
					produksicelup.tgltrans AS tglproduksi,
					produksicelupdetail.jumlah AS jumlahproduksi,
					CASE WHEN produksicelupdetail.transid IS NULL THEN 'Terima Order' ELSE 'Sudah Produksi' END AS mstatus,
					CASE WHEN LEFT(`order`.notrans,4) = 'ORLK' THEN 'LOKAL' ELSE 'EXPORT' END AS mstatusexp
				FROM
					`order`
				INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
				INNER JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
				LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
				LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
				LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
				LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
				LEFT JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
				LEFT JOIN produksicelup ON produksicelupdetail.transid = produksicelup.idtrans
				WHERE produksicelup.tgltrans BETWEEN :rptdatefrom AND :rptdateto";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
			SUM(produksicelupdetail.jumlah) AS tjumlahproduksi
			FROM
				`order`
			INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
			INNER JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
			LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
			LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
			LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
			LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
			LEFT JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
			LEFT JOIN produksicelup ON produksicelupdetail.transid = produksicelup.idtrans
				WHERE produksicelup.tgltrans BETWEEN :rptdatefrom AND :rptdateto";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotalgagal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
			SUM(produksicelupdetail.jumlah) AS tjumlahproduksi
			FROM
				`order`
			INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
			INNER JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
			LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
			LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
			LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
			LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
			LEFT JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
			LEFT JOIN produksicelup ON produksicelupdetail.transid = produksicelup.idtrans
				WHERE produksicelup.status=9 AND produksicelup.tgltrans BETWEEN :rptdatefrom AND :rptdateto";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotalselesai($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
			SUM(produksicelupdetail.jumlah) AS tjumlahproduksi
			FROM
				`order`
			INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
			INNER JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
			LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
			LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
			LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
			LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
			LEFT JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
			LEFT JOIN produksicelup ON produksicelupdetail.transid = produksicelup.idtrans
				WHERE produksicelup.status=0 AND produksicelup.tgltrans BETWEEN :rptdatefrom AND :rptdateto";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

