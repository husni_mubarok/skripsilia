<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey', 'kode', 'namapelanggan','alamat1','kodepos','tlp1','fax','email','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				pelanggan.idpelanggan AS idkey,
				CONCAT(LEFT(UPPER(pelanggan.namapelanggan),3), "-", LPAD(pelanggan.idpelanggan, 3, "0")) AS kode,
				pelanggan.noregistrasi,
				pelanggan.tglregistrasi,
				pelanggan.namapelanggan,
				pelanggan.alamat1,
				pelanggan.alamat2,
				pelanggan.kodepos,
				pelanggan.tlp1,
				pelanggan.tlp2,
				pelanggan.fax,
				pelanggan.email,
				pelanggan.kotaid,
				pelanggan.tipepelangganid,
				pelanggan.status,
				pelanggan.keterangan,
				CASE WHEN pelanggan.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN pelanggan.status = 0 THEN "Aktif" ELSE "Tidak Aktif" END AS descstatus
				FROM
				pelanggan) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['kode'], $aRow['namapelanggan'], $aRow['alamat1'], $aRow['kodepos'], $aRow['tlp1'], $aRow['fax'], $aRow['email'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

