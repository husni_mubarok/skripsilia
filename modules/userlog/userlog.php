<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User Log
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">User Log</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
      <div class="box-header">
        </div><!-- /.box-header -->
        <div class="box-body">
        <table id="userlog" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>USERNAME</th>
                <th>LOGIN DATE</th>
                <th>LOGOUT DATE</th>
            </tr>
            </thead>
        </table>        
		</div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row --> 
</section><!-- /.content -->
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#userlog').DataTable({
            "columns": [
                {"data": "username"},
                {"data": "logindate"},
                {"data": "logoutdate"}
            ],
            "processing": true,
            "serverSide": true,
			"paging": true,
            "ajax": {
                url: 'lib/Showuserlog.php',
                type: 'POST'
            }
        });
    });
</script>
