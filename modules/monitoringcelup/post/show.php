<?php
	$user = $_GET['user'];
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idtrans','namapelanggan','notrans','tgltrans','jadwalcelup','noproduksi','tglproduksi','namabarang','namawarnabarang','namaukuranbarang','jumlah','jumlahproduksi','jumlahsisa','mstatus'); 
	$sIndexColumn = 'idtrans';
	$sTable = '(SELECT
	`order`.idtrans,
	`order`.kodetrans,
	`order`.notrans,
	DATE_FORMAT(`order`.tgltrans, "%d/%m/%Y")AS tgltrans,
	`order`.keterangan AS keteranganh,
	`order`.pelangganid,
	pelanggan.namapelanggan,
	orderdetail.idtransdet,
	orderdetail.transid,
	orderdetail.barangid,
	orderdetail.satuan,
	orderdetail.jumlah,
	barang.kodebarang,
	barang.namabarang,
	barang.tipebarangid,
	tipebarang.namatipebarang,
	barang.warnabarangid,
	warnabarang.namawarnabarang,
	ukuranbarang.namaukuranbarang,
	barang.ukuranbarangid,
	DATE_FORMAT(
	`orderdetail`.jadwalcelup,
	"%m/%d/%Y"
	)AS jadwalcelup,
	IFNULL(orderdetail.jumlah, 0)- IFNULL(produksicelupdetail.jumlah, 0) AS jumlahsisa,
	produksicelup.notrans AS noproduksi,
	produksicelup.tgltrans AS tglproduksi,
	produksicelupdetail.jumlah AS jumlahproduksi,
	CASE
	WHEN produksicelupdetail.transid IS NULL AND orderdetail.jadwalcelup= "00/00/0000" THEN
	"Terima Order"
	ELSE 
	CASE
	WHEN produksicelupdetail.transid IS NULL AND orderdetail.jadwalcelup<> "00/00/0000" THEN
	"Belum Produksi"
	ELSE
	CASE WHEN  produksicelup.status=9 THEN 
	"Gagal Produksi"
	ELSE CASE WHEN  produksicelup.status=0 THEN
	"Sudah Produksi"
	END END END END AS mstatus
	FROM
	`order`
	INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
	INNER JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan
	LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
	LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
	LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
	LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang
	LEFT JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
	LEFT JOIN produksicelup ON produksicelupdetail.transid = produksicelup.idtrans,
				users
				WHERE users.username="'.$user.'" AND `order`.tgltrans BETWEEN users.begindate AND users.enddate) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}

		$row = array( $aRow['namapelanggan'], $aRow['notrans'], $aRow['tgltrans'], $aRow['jadwalcelup'], $aRow['noproduksi'], $aRow['tglproduksi'], $aRow['namabarang'], $aRow['namawarnabarang'], $aRow['namaukuranbarang'], $aRow['jumlah'], $aRow['jumlahproduksi'], $aRow['jumlahsisa'], $aRow['mstatus']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>
