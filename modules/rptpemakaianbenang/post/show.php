<?php
	$user = $_GET['user'];
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'tgltrans','jenisbenang','namaukuranbarang','namatipebarang','jumlah'); 
	$sIndexColumn = 'tgltrans';
	$sTable = '(SELECT
					DATE_FORMAT(
						`order`.tgltrans,
						"%d/%m/%Y"
					) AS tgltrans,
					CONCAT(
						barang.kodebarang, " ",
						barang.namabarang, " ",
						warnabarang.namawarnabarang
					) AS jenisbenang,
					ukuranbarang.namaukuranbarang,
					tipebarang.namatipebarang,
					SUM(orderdetail.jumlah) AS jumlah
				FROM
					`order`
				INNER JOIN orderdetail ON `order`.idtrans = orderdetail.transid
				LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
				LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
				LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
				LEFT JOIN ukuranbarang ON barang.ukuranbarangid = ukuranbarang.idukuranbarang,
				 users
				WHERE
					users.username = "'.$user.'"
				AND `order`.tgltrans BETWEEN users.begindate
				AND users.enddate
				GROUP BY
					`order`.tgltrans,
					barang.kodebarang,
					barang.namabarang,
					tipebarang.namatipebarang,
					warnabarang.namawarnabarang,
					ukuranbarang.namaukuranbarang) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}

		$row = array(  $aRow['tgltrans'], $aRow['jenisbenang'], $aRow['namaukuranbarang'], $aRow['namatipebarang'], $aRow['jumlah'] );
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>
