
<!-- Full Width Column -->
<div class="content-wrapper">
<div class="container">
  <!-- Content Header (Page header) -->
  <section class="content-header">
	<h1>
    <div id="type"></div>
	</h1>
	<ol class="breadcrumb">
	  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	  <li><a href="#">Index</a></li>
	  <li class="active">Welcome</li>
	</ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-6">
           <!-- TABLE: LATEST ORDERS -->
       
       
      </div><!-- /.box -->
        </div><!-- /.col -->
    
   <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-globe"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Terima Order</span>
             <a href="index.php?mod=order">Lihat Order</a>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Produksi</span> 
            
             <a href="index.php?mod=produksicelup">Lihat Produksi</a>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Monitoring</span>
            
          <a href="index.php?mod=monitoringcelup">Lihat Monitoring</a>
            
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  </section><!-- /.content -->
</div><!-- /.container -->
</div><!-- /.content-wrapper -->

<style>
#type {
    margin-bottom: 15px;
    font-size: 18px;
    font-weight: 200;
}
@media screen and (min-width: 768px) {
    #type {
        font-size: 23px;
    }
}
</style>


