<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','kode','namaukuranbarang','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				ukuranbarang.idukuranbarang AS idkey,
				CONCAT(LEFT(UPPER(ukuranbarang.namaukuranbarang),2), "-", LPAD(ukuranbarang.idukuranbarang, 2, "0")) AS kode,
				ukuranbarang.namaukuranbarang,
				ukuranbarang.status,
				CASE WHEN ukuranbarang.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN ukuranbarang.status = 0 THEN "Aktif" ELSE "Tidak Aktif" END AS descstatus
				FROM
				ukuranbarang) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['kode'], $aRow['namaukuranbarang'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

