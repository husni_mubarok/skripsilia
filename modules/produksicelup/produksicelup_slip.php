<?php
	$db=new Database();
	$purchaseorder = new Purchaseorder();
	$core=new Core();
	if (isset($_GET['key'])) {
		$key = $_GET['key'];
		$data = $purchaseorder->showListHeader($key);
		extract($data);
		$datadetail = $purchaseorder->showslipdetail($key);
		$datatotal = $purchaseorder->showsliptotal($key);
	}
?>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>

<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="Purchase%20Order_files/filelist.xml">
<style id="Book1_12617_Styles"><!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
.xl6312617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;
	background:#EDEDED;
	mso-pattern:black none;
	white-space:normal;}
.xl6412617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl6512617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl6612617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl6712617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl6812617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl6912617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7012617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7112617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7212617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7312617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7412617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7512617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7612617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:13.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7712617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7812617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7912617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl8012617
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
--></style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Book1_12617" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=885 class=xl6612617
 style='border-collapse:collapse;table-layout:fixed;width:664pt'>
 <col class=xl6612617 width=20 style='mso-width-source:userset;mso-width-alt:
 731;width:15pt'>
 <col class=xl6612617 width=100 style='mso-width-source:userset;mso-width-alt:
 3657;width:75pt'>
 <col class=xl6612617 width=307 style='mso-width-source:userset;mso-width-alt:
 11227;width:230pt'>
 <col class=xl6612617 width=89 style='mso-width-source:userset;mso-width-alt:
 3254;width:67pt'>
 <col class=xl6612617 width=51 style='mso-width-source:userset;mso-width-alt:
 1865;width:38pt'>
 <col class=xl6612617 width=99 style='mso-width-source:userset;mso-width-alt:
 3620;width:74pt'>
 <col class=xl6612617 width=86 style='mso-width-source:userset;mso-width-alt:
 3145;width:65pt'>
 <col class=xl6612617 width=133 style='mso-width-source:userset;mso-width-alt:
 4864;width:100pt'>
 <tr height=12 style='mso-height-source:userset;height:9.0pt'>
  <td height=12 class=xl6612617 width=20 style='height:9.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl6612617 width=133 style='width:100pt'></td>
 </tr>
 <tr height=13 style='mso-height-source:userset;height:9.75pt'>
  <td height=13 class=xl6612617 width=20 style='height:9.75pt;width:15pt'></td>
  <td colspan=7 rowspan=2 class=xl7512617 width=865 style='width:649pt'>PT.
  TROCON INDAH PERKASA</td>
 </tr>
 <tr height=13 style='mso-height-source:userset;height:9.75pt'>
  <td height=13 class=xl6612617 width=20 style='height:9.75pt;width:15pt'></td>
 </tr>
 <tr height=27 style='mso-height-source:userset;height:20.25pt'>
  <td height=27 class=xl6612617 width=20 style='height:20.25pt;width:15pt'></td>
  <td colspan=7 class=xl7612617 width=865 style='width:649pt'>PURCHASE ORDER</td>
 </tr>
 <tr height=27 style='mso-height-source:userset;height:20.25pt'>
  <td height=27 class=xl6612617 width=20 style='height:20.25pt;width:15pt'></td>
  <td class=xl6712617 width=100 style='width:75pt'>Pemasok</td>
  <td class=xl6812617 width=307 style='border-top:none;width:230pt'>: <?php echo $suppliername; ?></td>
  <td class=xl6812617 width=89 style='border-top:none;width:67pt'>&nbsp;</td>
  <td class=xl6812617 width=51 style='border-top:none;width:38pt'>&nbsp;</td>
  <td class=xl6812617 width=99 style='border-top:none;width:74pt'>Nomor</td>
  <td colspan=2 class=xl8012617 width=219 style='width:165pt'>:  <?php echo $notrans; ?></td>
 </tr>
 <tr height=27 style='mso-height-source:userset;height:20.25pt'>
  <td height=27 class=xl6612617 width=20 style='height:20.25pt;width:15pt'></td>
  <td class=xl6712617 width=100 style='width:75pt'>TOP</td>
  <td class=xl6912617 width=307 style='width:230pt'>: <?php echo $top; ?></td>
  <td class=xl6912617 width=89 style='width:67pt'></td>
  <td class=xl6912617 width=51 style='width:38pt'></td>
  <td class=xl6912617 width=99 style='width:74pt'>Tanggal</td>
  <td colspan=2 class=xl6712617 width=219 style='width:165pt'>:  <?php echo $datetrans; ?></td>
 </tr>
 <tr height=8 style='mso-height-source:userset;height:6.0pt'>
  <td height=8 class=xl6612617 width=20 style='height:6.0pt;width:15pt'></td>
  <td class=xl6712617 width=100 style='width:75pt'></td>
  <td class=xl7012617 width=307 style='width:230pt'></td>
  <td class=xl7012617 width=89 style='width:67pt'></td>
  <td class=xl7012617 width=51 style='width:38pt'></td>
  <td class=xl7012617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl6612617 width=133 style='width:100pt'></td>
 </tr>
 <tr class=xl7112617 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl7112617 width=20 style='height:17.25pt;width:15pt'></td>
  <td class=xl6312617 width=100 style='width:75pt'>Kode Material</td>
  <td class=xl6312617 width=307 style='border-left:none;width:230pt'>Nama
  Material</td>
  <td class=xl6312617 width=89 style='border-left:none;width:67pt'>Satuan</td>
  <td class=xl6312617 width=51 style='border-left:none;width:38pt'>Qty</td>
  <td class=xl6312617 width=99 style='border-left:none;width:74pt'>Harga</td>
  <td class=xl6312617 width=86 style='border-left:none;width:65pt'>Potongan</td>
  <td class=xl6312617 width=133 style='border-left:none;width:100pt'>Total</td>
 </tr>
  <?php
	$i=1;
	if($datadetail !=0){
	foreach ($datadetail as $value) {
	extract($value);
 ?>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6412617 width=100 style='border-top:none;width:75pt'><?php echo $inventorycode; ?></td>
  <td class=xl6412617 width=307 style='border-top:none;border-left:none;
  width:230pt'><?php echo $inventoryname; ?></td>
  <td class=xl6412617 width=89 style='border-top:none;border-left:none;
  width:67pt'>&nbsp;<?php echo $unit; ?></td>
  <td class=xl6412617 width=51 style='border-top:none;border-left:none;
  width:38pt'>&nbsp;<?php echo $quantity; ?></td>
  <td class=xl6412617 width=99 style='border-top:none;border-left:none;
  width:74pt'>&nbsp;<?php echo number_format($unitprice); ?></td>
  <td class=xl6412617 width=86 style='border-top:none;border-left:none;
  width:65pt'><?php echo $discount; ?></td>
  <td class=xl6512617 width=133 style='border-top:none;border-left:none;
  width:100pt'><?php echo number_format( $amount); ?></td>
 </tr>
   <?php
	$i++;
		}
		} 
 ?>
 <?php
	$i=1;
	if($datatotal !=0){
	foreach ($datatotal as $value) {
	extract($value);
 ?>  
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl7912617 width=86 style='border-top:none;width:65pt'>Total</td>
  <td class=xl7912617 width=133 style='border-top:none;border-left:none;
  width:100pt'>&nbsp;<?php echo number_format( $tamount); ?></td>
 </tr>
 <?php
	$i++;
		}
		} 
 ?>  
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'>Keterangan</td>
  <td colspan=6 class=xl7712617 width=765 style='width:574pt'>: <?php echo $remark; ?></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl6612617 width=133 style='width:100pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl7212617 width=133 style='width:100pt'>Dibuat Oleh</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td rowspan=3 class=xl7812617 width=133 style='border-top:none;width:100pt'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td colspan=2 class=xl7312617 width=407 style='width:305pt'></td>
  <td class=xl7312617 width=89 style='width:67pt'></td>
  <td class=xl7312617 width=51 style='width:38pt'></td>
  <td class=xl7312617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl7412617 width=133 style='border-top:none;width:100pt'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td class=xl6612617 width=100 style='width:75pt'></td>
  <td class=xl6612617 width=307 style='width:230pt'></td>
  <td class=xl6612617 width=89 style='width:67pt'></td>
  <td class=xl6612617 width=51 style='width:38pt'></td>
  <td class=xl6612617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl6612617 width=133 style='width:100pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6612617 width=20 style='height:15.0pt;width:15pt'></td>
  <td colspan=2 class=xl7312617 width=407 style='width:305pt'></td>
  <td class=xl7312617 width=89 style='width:67pt'></td>
  <td class=xl7312617 width=51 style='width:38pt'></td>
  <td class=xl7312617 width=99 style='width:74pt'></td>
  <td class=xl6612617 width=86 style='width:65pt'></td>
  <td class=xl6612617 width=133 style='width:100pt'></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=20 style='width:15pt'></td>
  <td width=100 style='width:75pt'></td>
  <td width=307 style='width:230pt'></td>
  <td width=89 style='width:67pt'></td>
  <td width=51 style='width:38pt'></td>
  <td width=99 style='width:74pt'></td>
  <td width=86 style='width:65pt'></td>
  <td width=133 style='width:100pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>

