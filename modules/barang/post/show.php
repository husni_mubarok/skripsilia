<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey', 'kode', 'kodebarang','namabarang','namatipebarang','namaukuranbarang','namawarnabarang','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				barang.idbarang AS idkey,
				CONCAT(LEFT(UPPER(barang.namabarang),1), LPAD(barang.idbarang, 3, "0")) AS kode,
				barang.kodebarang,
				barang.namabarang,
				barang.satuan,
				barang.tipebarangid,
				tipebarang.namatipebarang,
				barang.warnabarangid,
				warnabarang.namawarnabarang,
				ukuranbarang.namaukuranbarang,
				barang.ukuranbarangid,
				CASE WHEN barang.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN barang.status = 0 THEN "Aktif" ELSE "Not Tidak Aktif" END AS descstatus
				FROM
				barang
				LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
				LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
				LEFT JOIN ukuranbarang ON barang.ukuranbarangid= ukuranbarang.idukuranbarang) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['kode'], $aRow['namabarang'], $aRow['namatipebarang'], $aRow['namaukuranbarang'], $aRow['namawarnabarang'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

