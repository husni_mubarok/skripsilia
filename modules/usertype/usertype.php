<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User Type
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Utility</a></li>
    <li class="active">User Type</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
      <div class="box-header">
        </div><!-- /.box-header -->
        <div class="box-body">
		<script type="text/javascript" language="javascript" >
			var dTable;
			$(document).ready(function() {
				dTable = $('#datagrid').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": true,
					"sAjaxSource": "lib/showUsertype.php",
					"sServerMethod": "POST",
					"columnDefs": [
					{ "orderable": false, "targets": 0, "searchable": false },
					{ "orderable": true, "targets": 1, "searchable": true },
					]
				} );
				
				$('#datagrid').removeClass( 'display' ).addClass('table table-striped table-bordered');
				$('#datagrid tfoot th').each( function () {
					
					if( $(this).text() != "Action" ){
						var title = $('#datagrid thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Search '+title+'" class="form-control" />' );
					}
				} );
				
			} );
		</script>
        
        <div class="box-header">
          <button onClick="showModals()" class="btn btn-success" style="margin-left:-10px;margin-top:-15px">Add New</button>
        </div><!-- /.box-header -->
        <table id="datagrid"  class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>User Type</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>		<!-- Modal Popup -->
		<div class="modal fade" id="myModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">User Type</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" id="formData" action="">
							<input type="hidden" class="form-control" id="idusertype" name="idusertype">
							<input type="hidden" class="form-control" id="type" name="type">
							<div class="form-group">
								<label for="real_name" class="col-sm-2 control-label">User Type</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="usertypename" name="usertypename" >
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" onClick="submitUser()" class="btn btn-primary">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<script>
			function showModals( idusertype )
			{
				clearModals();
				if( idusertype )
				{
					clearModals();
					$.ajax({
						type: "POST",
						url: "lib/Usertype.php",
						dataType: 'json',
						data: {idusertype:idusertype,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalData( res );
						}
					});
				}
				else
				{
					$("#myModalLabel").html("New User Type");
					$("#type").val("new"); 
					waitingDialog.hide();
					$("#myModals").modal("show");
				}
			}
			
			function setModalData( data )
			{
				clearModals();
				$("#myModalLabel").html("Edit User Type");
				$("#myModalLabel").html(data.real_name);
				$("#idusertype").val(data.idusertype);
				$("#type").val("edit");
				$("#usertypename").val(data.usertypename);
				$("#myModals").modal("show");
				waitingDialog.hide();	
			}
			
			function deleteUser( idusertype )
			{
				clearModals();
				$.ajax({
					type: "POST",
					url: "lib/Usertype.php",
					dataType: 'json',
					data: {idusertype:idusertype,type:"get"},
					success: function(data) {
						$("#myModalLabel").html("Delete User Type");
						$("#idusertype").val(data.idusertype);
						$("#usertypename").val(data.usertypename);
						$("#type").val("delete");
						$("#usertypename").val(data.usertypename).attr("disabled","false");
						$("#myModals").modal("show");
						waitingDialog.hide();			
					}
				});
			}

			function submitUser()
			{
				var x = document.getElementById("usertypename").value; 
				if (x == '') {
				  $('#formData').formValidation('validate');
				  } else {
						var formData = $("#formData").serialize();
						waitingDialog.show();
						$.ajax({
							type: "POST",
							url: "lib/Usertype.php",
							dataType: 'json',
							data: formData,
							success: function(data) {
								dTable.ajax.reload(); // Untuk Reload Tables secara otomatis
								waitingDialog.hide();	
								$('#myModals').modal('hide');
							}
						});
	 			}
			}
			
			function clearModals()
			{
				$("#removeWarning").hide();
				$("#idusertype").val("").removeAttr( "disabled" );
				$("#usertypename").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
		</script>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row --> 
</section><!-- /.content -->
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#formData').formValidation({
        err: {
            container: 'tooltip'
        },
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            usertypename: {
                validators: {
                    stringLength: {
                        enabled: false,
                        min: 4,
                        message: 'The item type must be more than 5 characters'
                    },
                    notEmpty: {
                        message: 'The user type is required'
                    }
                }
            }
        }
    });
});
//Textbox event
$("#usertypename").keyup(function(event){
    if(event.keyCode == 13){
        submitUser()
    }
});
</script>







