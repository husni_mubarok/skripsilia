<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','namatipebarang','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				tipebarang.idtipebarang AS idkey,
				tipebarang.namatipebarang,
				tipebarang.status,
				CASE WHEN tipebarang.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN tipebarang.status = 0 THEN "Aktif" ELSE "Tidak Aktif" END AS descstatus
				FROM
				tipebarang) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['idkey'], $aRow['namatipebarang'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

