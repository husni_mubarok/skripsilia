<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','kode','namawarnabarang','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				warnabarang.idwarnabarang AS idkey,
				CONCAT(LEFT(UPPER(warnabarang.namawarnabarang),3), "-", LPAD(warnabarang.idwarnabarang, 3, "0")) AS kode,
				warnabarang.namawarnabarang,
				warnabarang.status,
				CASE WHEN warnabarang.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN warnabarang.status = 0 THEN "Aktif" ELSE "Tidak Aktif" END AS descstatus
				FROM
				warnabarang) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['kode'], $aRow['namawarnabarang'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

